require 'colorize'

new_cola_purchase_using_recycled_caps =0
@remainder_caps_from_cola_purchase =0
new_cola_purchase_using_recycled_bottles =0
@remainder_bottles_from_cola_purchase =0
new_cola_purchase_from_recycled_caps_bottles = 0
@total_new_cola_purchase_from_recycled_caps_bottles_fp =0
@total_new_cola_purchase_using_recycled_caps_fp = 0
@total_new_cola_purchase_using_recycled_bottles_fp = 0

def cola_purchase(investment)
  cola_price =2 
  @total_number_of_colas = investment/cola_price 
  recycle_products_generated(@total_number_of_colas) 
end

def recycle_products_generated(num_of_colas)
  @total_number_of_caps = @total_number_of_colas 
  @total_number_of_bottles = @total_number_of_colas 
  calculate_the_new_cola_purchase(@total_number_of_caps,@total_number_of_bottles)
end

def calculate_the_new_cola_purchase(caps,bottles) 
  cap_exchange =4
  bottle_exchange =2 
  new_cola_purchase_using_recycled_caps = caps/cap_exchange 
  @remainder_caps_from_cola_purchase = caps.modulo(cap_exchange) 
  new_cola_purchase_using_recycled_bottles = bottles/bottle_exchange 
  @remainder_bottles_from_cola_purchase = bottles.modulo(bottle_exchange) 
  new_cola_purchase_from_recycled_caps_bottles = new_cola_purchase_using_recycled_caps + new_cola_purchase_using_recycled_bottles
  new_bottles_created =  @remainder_bottles_from_cola_purchase +  new_cola_purchase_from_recycled_caps_bottles
  new_caps_created = @remainder_caps_from_cola_purchase + new_cola_purchase_from_recycled_caps_bottles
  @total_new_cola_purchase_from_recycled_caps_bottles_fp += new_cola_purchase_using_recycled_caps + new_cola_purchase_using_recycled_bottles
  @total_new_cola_purchase_using_recycled_caps_fp += new_cola_purchase_using_recycled_caps
  @total_new_cola_purchase_using_recycled_bottles_fp += new_cola_purchase_using_recycled_bottles
  calculate_number_of_bottles_with_initial_investment(new_caps_created,new_bottles_created,new_cola_purchase_from_recycled_caps_bottles)
end

def calculate_number_of_bottles_with_initial_investment(caps,bottles,colas_new)
  if (colas_new == 0) 
    puts "----------- Breakdown of pop bottles------------"
    puts "                                                "
    puts "The total number of pop bottles you can purchase through recycling is = #{@total_new_cola_purchase_from_recycled_caps_bottles_fp}".colorize(:red) 
    puts "Pop obtained through bottle recycling is = #{@total_new_cola_purchase_using_recycled_bottles_fp} ".colorize(:red) 
    puts "Pop obtained through caps recycling is = #{@total_new_cola_purchase_using_recycled_caps_fp}".colorize(:red) 
    puts "                                                "
    puts "----------- Remaining Caps and Bottles----------"
    puts "                                                "
    puts "Remaining number of caps = #{@remainder_caps_from_cola_purchase}"
    puts "Remaining number of bottles = #{@remainder_bottles_from_cola_purchase}"
    puts "                                                " 
    puts "Is there a second customer (yes/no) ?" .colorize(:blue) 
    get_customer_value = gets.chomp.downcase
      if get_customer_value == "yes"
        @total_new_cola_purchase_from_recycled_caps_bottles_fp =0
        @total_new_cola_purchase_using_recycled_caps_fp =0 
        @total_new_cola_purchase_using_recycled_bottles_fp =0
        repl_customer_quote
      else
        puts "Thank you for using this service"
      end
  else
    calculate_the_new_cola_purchase(caps,bottles)
  end
end


def repl_customer_quote
  puts "                                                "
  puts "what is the investment you would like to make"
  @investment_value = gets.chomp.to_i
  cola_purchase(@investment_value)
end 

repl_customer_quote
